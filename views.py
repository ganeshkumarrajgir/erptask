from django.shortcuts import render

# Create your views here.
import json
from django.http import JsonResponse
from newblog.models import register_data
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
@csrf_exempt
def newuser(request):
  if request.method=="POST":
     # user=register_data()
      data=json.loads(request.body)
      register_data.objects.create(name=data['name'],email=data['email'],
                                    blog_title=data['blogtitle'],
                                    blog_abstract=data['abstract'],
                                    blog_description=data['content'])
    #   register_data.objects.bulk_create([register_data(name=data['name'],
    #                                     email=data['email'],
    #                                     blog_title=data['blogtitle'],
    #                                     blog_abstract=data['abstract'],
    #                                     blog_description=data['content'] ),
    #                                     register_data(name=data['name'],
    #                                     email=data['email'],
    #                                     blog_title=data['blogtitle'],
    #                                     blog_abstract=data['abstract'],
    #                                     blog_description=data['content'] )])
    #   user.name=data['name']
    #   user.email=data['email']
    #   user.blog_title=data['blogtitle']
    #   user.blog_abstract=data['abstract']
    #   user.blog_description=data['content']
    #   user.save()
      return JsonResponse(data,status=200,safe=False)
  else:
        return JsonResponse("Data Not Registered",status=304,safe=False)


def blog_name(request):
    if request.method=="GET":
        length=register_data.objects.filter(status='ndel').count()  #ndel= not delete
        if length==0:
            return JsonResponse("Table is Empty",status=200,safe=False)
        else:    
            user=register_data.objects.filter(status='ndel').values('id','blog_title').order_by('blog_title')
            result=list(user)
            return JsonResponse(result,status=200,safe=False)

@csrf_exempt
def about_blog(request):
    if request.method=="POST":
        data=json.loads(request.body)
        x=data['id']
        print(x)
        value=register_data.objects.filter(id=x).values()
        result=list(value)
    return JsonResponse(result,status=200,safe=False)   

@csrf_exempt
def delete(request):
    if request.method=="POST":
        data=json.loads(request.body)
        x=data['id']
        register_data.objects.filter(id=x).update(status='del')
        # result=register_data.objects.get(id=x)
        # result.status='del'
        # result.save()
        return JsonResponse("Data Deleted",status=200,safe=False)    
    else:
        return JsonResponse("Not Deleted ",status=304,safe=False)

def blog_description(request):
    if request.method=="GET":
        data=json.loads(request.body)
        x=data['id']
        item=register_data.objects.filter(id=x).values('blog_description')
        value=list(item)
        return JsonResponse(value,status=200,safe=False)

@csrf_exempt
def update(request):
    if request.method=="PUT":
        data=json.loads(request.body)
        x=data['id']
        y=data['newcontent']
        register_data.objects.filter(id=x).update(blog_description=y)
       # result=register_data.objects.get(id=x)
        #result.blog_description=data['newcontent']
        #print(result.blog_description)
        #result.save()
        return JsonResponse("Updated Successfully",status=200,safe=False)
    else:
        return JsonResponse("Not Updated",status=304,safe=False)    

def count(request):
    if request.method=="GET":
        length=register_data.objects.count()
        return JsonResponse(length,status=200,safe=False)    

def deleted_data(request):
    if request.method=="GET":
        length=register_data.objects.exclude(status='ndel').count()
        if length==0:
            return JsonResponse("No Deleted Data In Table",status=200,safe=False)
        else:    
            data=register_data.objects.exclude(status='ndel').values()
            result=list(data)
            return JsonResponse(result,status=200,safe=False)

def retrive_data(request):
    if request.method=="GET":
        length=register_data.objects.filter(status='del').count()
        if length==0:
            return JsonResponse("No Data To Retrive",status=200,safe=False)
        else:
            register_data.objects.filter(status='del').update(status='ndel')
            return JsonResponse("Data Retrived",status=200,safe=False)  

def reverse(request):
    if request.method=="GET":
        data=register_data.objects.values().reverse().order_by("id")
        values=list(data)
        return JsonResponse(values,status=200,safe=False)

def union(request):
    if request.method=="GET":
        set1=register_data.objects.values('name')
        set2=register_data.objects.values('blog_title')
        set3=set1.union(set2)
        values=list(set3)
        return JsonResponse(values,status=200,safe=False)

def in_bulk(request):   # it will return the values in primary key column
    if request.method=="GET":
        data=register_data.objects.in_bulk()
        value=list(data)
        item=register_data.objects.values().latest('blog_title')
        #item1=list(item)
        print(item)
        return JsonResponse(item,status=200,safe=False)       



