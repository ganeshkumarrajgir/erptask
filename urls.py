from django.urls import path
from .import views

urlpatterns=[
    path("register/",views.newuser,name='newuser'),
    path("allblogs/",views.blog_name,name='blog_name'),
    path("aboutblog/",views.about_blog,name='about_blog'),
    path("delete/",views.delete,name='delete'),
    path("blog_description/",views.blog_description,name='blog_description'),
    path("update/",views.update,name="update"),
    path("count/",views.count,name="count"),
    path("deleted_data/",views.deleted_data,name="deleted_data"),
    path("retrive_data/",views.retrive_data,name="retrive"),
    path("reverse/",views.reverse,name='reverse'),
    path("union/",views.union,name='union'),
    path("in_bulk/",views.in_bulk,name='inbulk')
    ]