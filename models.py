from django.db import models

# Create your models here.
class register_data(models.Model):
    name=models.CharField(max_length=30)
    email=models.EmailField(max_length=60)
    blog_title=models.CharField(max_length=60)
    blog_abstract=models.CharField(max_length=200)
    blog_description=models.CharField(max_length=700)
    status=models.CharField(max_length=30,default='ndel') #ndel = not delete